'use strict';

var React = require ('react');
var ReactNative = require ('react-native');
 var STVTopStoriesSearchPage = require ('./STVTopStoriesSearchPage');

var styles = ReactNative.StyleSheet.create({
  text: {
    color: 'black',
    backgroundColor: 'white',
    fontSize: 30,
    margin: 80
  },
  container: {
    flex: 1
  }
});

class STVTopStoriesApp extends React.Component {
  render() {
    return (
      <ReactNative.NavigatorIOS
        style={styles.container}
        initialRoute={{
          title: 'STV Top Stories',
          component: STVTopStoriesSearchPage,
        }}/>
    );
  }
}
        

ReactNative.AppRegistry.registerComponent('STVTopStories', function() { return STVTopStoriesApp });