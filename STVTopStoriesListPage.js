'use strict';
 
import React, { Component } from 'react'
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  ListView,
  TextInput,
  Text,
  WebView, 
  Linking,
   AlertIOS
} from 'react-native';

var styles = StyleSheet.create({	
  thumb: {
    width: 80,
    height: 80,
    marginRight: 10
  },
  listViewContainer: {	    
	    marginTop: 10
	  },
  textContainer: {
    flex: 1
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  price: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#48BBEC'
  },
  subheadline: {
    fontSize: 12,
    fontWeight: 'normal',
    color: '#48BBEC'
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#656565'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10
  },
  datePublished: {
  	fontSize: 12,
  	marginLeft : 10,
  	color: '#101010'
  },
  author: {
  	fontSize: 12,
  	fontWeight: 'bold',
  	color: '#101010'
  },
  
  searchBarflowRight: {
  flexDirection: 'row',
  marginTop:65,
  marginLeft:10,
  marginRight:10,
  marginBottom:10,
  alignItems: 'center',
  alignSelf: 'stretch'
},
flowRight: {
  flexDirection: 'row',

  marginLeft:10,  
  alignItems: 'center',
  alignSelf: 'stretch'
},
searchInput: {
	  height: 36,
	  padding: 4,
	  marginRight: 5,
	  flex: 4,
	  fontSize: 18,
	  borderWidth: 1,
	  borderColor: '#48BBEC',
	  borderRadius: 8,
	  color: '#48BBEC'
},
button: {
	  height: 36,
	  flex: 1,
	  flexDirection: 'row',
	  backgroundColor: '#48BBEC',
	  borderColor: '#48BBEC',
	  borderWidth: 1,
	  borderRadius: 8,
	  marginBottom: 10,
	  alignSelf: 'stretch',
	  justifyContent: 'center'
	},
	buttonText: {
  fontSize: 18,
  color: 'white',
  alignSelf: 'center'
	}	
});

class STVTopStoriesListPage extends Component {
 
  constructor(props) {
    super(props);
    var dataSource = new ListView.DataSource(
      {rowHasChanged: (r1, r2) => r1.id !== r2.id});    
    this.state = {
      filteredResults : [],
      dataSource: dataSource.cloneWithRows(this.props.results),
      searchResultsFound : true,
      searchString :''
    };
  }

  	rowPressed(storyURL) {
 	
 	Linking.openURL(storyURL);
	}
 	
 	stringToDate(_date,_format,_delimiter)
	{
        var formatLowerCase=_format.toLowerCase();
        var formatItems=formatLowerCase.split(_delimiter);
        var dateItems=_date.split(_delimiter);
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
        return formatedDate;
	}

	onSearchTextChanged(event) {
	  this.setState({ searchString: event.nativeEvent.text });
	}

	onSearchPressed() 
	{
		console.log('searchString = '+ this.state.searchString);
		if(this.state.searchString.length > 0)
		{
			var searchString = this.state.searchString.toLowerCase();
			var newFilteredResults = this.props.results.filter(function(searchResult) 
															{
																console.log ('Index =' + searchResult.title.toLowerCase().
			 													indexOf(searchString));
																return searchResult.title.toLowerCase().
			 													indexOf(searchString) > -1;});
				if(newFilteredResults.length > 0 )
				{
					var dataSource = new ListView.DataSource(
      							{rowHasChanged: (r1, r2) => r1.id !== r2.id});    
				this.setState({
						filteredResults : newFilteredResults,
	      				dataSource: dataSource.cloneWithRows(newFilteredResults),
	      				searchResultsFound : true,
	      				searchString :searchString
	    		});
      		}
      		else
			{				
				AlertIOS.alert( 'No results found', 'Please try again');				
				var dataSource = new ListView.DataSource(
      							{rowHasChanged: (r1, r2) => r1.id !== r2.id});    
				this.setState({
							filteredResults : [],
		      				dataSource: dataSource.cloneWithRows(this.props.results),
		      				searchResultsFound : false,
		      				searchString :''
	    		});
			}	  
		}
		else
		{
			var dataSource = new ListView.DataSource(
      							{rowHasChanged: (r1, r2) => r1.id !== r2.id});    			
			this.setState({
							filteredResults : [],
		      				dataSource: dataSource.cloneWithRows(this.props.results),
		      				searchResultsFound : false,
		      				searchString :''
	    		});
		}	  
	}


  renderRow(rowData, sectionID, rowID) {
  	var authorDictionary;
  	var authorName
  	if(rowData.byline){
  		authorDictionary = rowData.byline[0];
 		authorName = (authorDictionary.firstname) + ' ' + (authorDictionary.lastname);
 	}
 	else
 	{
 		authorName	= '';
 	}
 	// var dateObject = this.stringToDate(rowData.published.split('T')[0],'yyyy-mm-dd','-');
 	var publishedDate = rowData.published.split('T')[0];

  return (
    <TouchableHighlight onPress={() => this.rowPressed(rowData.links.self)}
        underlayColor='#dddddd'>
      <View>
        <View style={styles.rowContainer}>
          <Image style={styles.thumb} source={{ uri: rowData.image.renditions.small }} />
          <View  style={styles.textContainer}>
            
            <Text style={styles.title}
                  numberOfLines={2}>{rowData.title}</Text>
                  <Text style={styles.subheadline}>{rowData.subHeadline}</Text>
                  <View style={styles.flowRight}>
  					<Text style={styles.author}>{authorName}</Text>
  					<Text style={styles.datePublished}>{publishedDate}</Text>
	</View>   
          </View>
        </View>
        <View style={styles.separator}/>
      </View>
    </TouchableHighlight>
  );
}
 
  render() {
    return (
    	<View>
    		<View style={styles.searchBarflowRight} >
  				<TextInput
				    style={styles.searchInput}
				    value={this.state.searchString}
				    onChange={this.onSearchTextChanged.bind(this)}
				    placeholder='Search for a story'/>
				  <TouchableHighlight style={styles.button}
				  onPress={this.onSearchPressed.bind(this)}
				      underlayColor='#99d9f4'>
				    <Text style={styles.buttonText}>Go</Text>
				  </TouchableHighlight>
				</View>
		<View style={styles.listViewContainer}>	     		
      	<ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderRow.bind(this)}/>
        </View>
        </View>
    );
  }
 
}
module.exports = STVTopStoriesListPage;