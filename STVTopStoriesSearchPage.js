	'use strict';

	import React, { Component } from 'react'
	import {
	  StyleSheet,
	  Text,
	  TextInput,
	  View,
	  TouchableHighlight,
	  ActivityIndicator,
	  Image
	} from 'react-native';
	var STVTopStoriesListPage = require('./STVTopStoriesListPage');

	var styles = StyleSheet.create({
	  description: {
	    marginBottom: 20,
	    fontSize: 18,
	    textAlign: 'center',
	    color: '#656565'
	  },
	  container: {
	    padding: 30,
	    marginTop: 65,
	    alignItems: 'center'
	  },
	  flowRight: {
	  flexDirection: 'row',
	  alignItems: 'center',
	  alignSelf: 'stretch'
	},
	buttonText: {
	  fontSize: 18,
	    height: 18,
	  color: 'white',
	  alignSelf: 'center'
	},
	button: {
	  height: 36,
	  flex: 1,
	  flexDirection: 'row',
	  backgroundColor: '#48BBEC',
	  borderColor: '#48BBEC',
	  borderWidth: 1,
	  borderRadius: 8,
	  marginBottom: 10,
	  alignSelf: 'stretch',
	  justifyContent: 'center'
	},
	searchInput: {
	  height: 36,
	  padding: 4,
	  marginRight: 5,
	  flex: 4,
	  fontSize: 18,
	  borderWidth: 1,
	  borderColor: '#48BBEC',
	  borderRadius: 8,
	  color: '#48BBEC'
	}
	});

	function urlForQueryAndPage(key, value, pageNumber) {
	  
	  return 'https://news.api.stv.tv/v1.1/mobile/3057?relations=image,section,byline' ;
	};

	class STVTopStoriesSearchPage extends Component {


	_executeQuery(query) {
	  console.log(query);
	  this.setState({ isLoading: true });
	  fetch(query)
	  .then(response => response.json())
	  .then(json => this._handleResponse(json))
	  .catch(error =>
	     this.setState({
	      isLoading: false,
	      message: 'Something bad happened ' + error
	   }));
	}
	_handleResponse(response) {
	  this.setState({ isLoading: false , message: '' });
	  if (response.success) {
	    this.props.navigator.push({
	  title: 'Top Stories',
	  component: STVTopStoriesListPage,
	  passProps: {results: response.results}
	});
	  } else {
	    this.setState({ message: 'No Results found.Please try again.'});
	  }
	}
	 
	onSearchPressed() {
	  var query = urlForQueryAndPage('place_name', this.state.searchString, 1);
	  this._executeQuery(query);
	}
		constructor(props) {
	  super(props);
	  this.state = {
	    searchString: 'london',
	    isLoading : false,
	    message : ''
	  };
	}
	onSearchTextChanged(event) {
	  this.setState({ searchString: event.nativeEvent.text });
	}
	 render() {
	 	var spinner = this.state.isLoading ?
	  ( <ActivityIndicator
	      size='large'/> ) :
	  ( <View/>);

	    return (
	      <View style={styles.container}>	     
		 <View style={styles.flowRight}>
				<TouchableHighlight style={styles.button}
	  			onPress={this.onSearchPressed.bind(this)}
	      		underlayColor='#99d9f4'>
	    		<Text style={styles.buttonText}>Get Top Stories</Text>
	  			</TouchableHighlight>
	      </View>   
	      {spinner}  
	      <Text style={styles.description}>{this.state.message}</Text> 
	      </View>
	    );
	  }
	}

	module.exports = STVTopStoriesSearchPage;